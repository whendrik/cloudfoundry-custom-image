# CloudFoundry custom image

## 1 - Install the `ibmcloud` CLI

Install the [IBM Cloud CLI](https://cloud.ibm.com/docs/cli?topic=cli-install-ibmcloud-cli), with the [container registry](https://cloud.ibm.com/docs/cli?topic=cli-install-devtools-manually#idt-install-container-registry-cli-plugin) plugin. Also the `cf` plugin.

## 2 - Login with the `ibmcloud cf`

Login using the `ibmcloud login --sso` command, and set the right [target](https://cloud.ibm.com/docs/cloud-foundry-public?topic=cloud-foundry-public-endpoints) and space.

```
ibmcloud login --sso

ibmcloud target --cf-api api.us-south.cf.cloud.ibm.com -o "<YOUR ORG>"  -s dev
```

## 3 - Check, and create if necessary, a registry namespace

Check current namespaces with (`ibmcloud cr --help` for help to create one);
```
ibmcloud cr namespaces
```

## 4 - Create image with `Dockerfile`

Say in our current directory there is a `Dockerfile`, we can create a image in the registry, where

- `willemh` refers to my namespace
- `dofa:2` refers to my `tag:version`

```
ibmcloud cr build --tag us.icr.io/willemh/dofa:2 .
```

The `Dockerfile` is expected to have a `EXPOSE` defined with 1 port, assigned to be the port of the web app.

## 5 - Use the custom image to push a app

You need to create a [APIKEY](https://cloud.ibm.com/docs/account?topic=account-userapikey#create_user_key), and either have it ready to paste the value or store it in a environment variable `CF_DOCKER_PASSWORD`


```
ibmcloud cf push customimage23453245002 --docker-image=us.icr.io/willemh/dofa:2 --docker-username iamapikey
```

